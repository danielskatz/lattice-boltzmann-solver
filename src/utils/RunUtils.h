/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#pragma once

#include "FileUtils.h"

namespace LBM
{
  struct RunIO
  {
    std::string input;
    // This is a full path down to the filename, with no extension
    // The intent is for the filename to be a prefix.
    // You can add numbers, or other identify to the file name, then your extension
    //  - For example, wtith output_base_path set to /some/path/to/run
    //  - Can now write out things like
    //      - /some/path/to/run.vtr
    //      - /some/path/to/run0.vtr
    //      - /some/path/to/run_inlet_surface.vtp
    //      - /some/path/to/run.flo
    std::string output_base_path;
  };

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4251 )
  // Explicit template exports.
  LBM_EXP template class LBM_DECL std::allocator<LBM::RunIO>;
  LBM_EXP template class LBM_DECL std::vector<LBM::RunIO, std::allocator<LBM::RunIO>>;
#pragma warning( pop )
#endif

  class LBM_DECL RunConfig
  {
  public:
    RunConfig();
    virtual ~RunConfig();

    void PrintUsage();
    void ParseArgs(int argc, char* argv[]);

    std::string GetITKDir() { return m_itk_dir; }
    const std::vector<RunIO>& GetITKRuns(std::string const& root = "");
    const std::vector<RunIO>& GetITKDirRuns(std::string const& sub_dir = "");

    std::string GetLegacyDir() { return m_legacy_dir; }
    const std::vector<RunIO>& GetLegacyRuns(std::string const& root = "");
    const std::vector<RunIO>& GetLegacyDirRuns(std::string const& sub_dir = "");

    std::string GetVerificationDir() { return m_verification_dir; }
    const std::vector<RunIO>& GetVerificationRuns(std::string const& root = "");
    const std::vector<RunIO>& GetVerificationDirRuns(std::string const& sub_dir = "");

  protected:
    bool ParseRunConfig();

    std::string m_itk_dir;
    std::string m_legacy_dir;
    std::string m_verification_dir;
    std::vector<RunIO> m_legacy_runs;
    std::vector<RunIO> m_runs;
  };
}
