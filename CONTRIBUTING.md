Contributing to LBM
===================

Welcome to the Kitware Lattice Boltzmann Solver ! We're excited you're here and want to contribute.

This article documents how to contribute improvements to LBM.

Setup
-----

Before you begin, perform initial setup:

  1. [Register for a Kitware GitLab](https://gitlab.kitware.com/users/sign_in) account.
  2. Create a Fork of this project under your account.
  
Workflow
--------

LBM development uses a branchy workflow based on topic branches.
This corresponds to the *Fork & Pull Model*.
Our collaboration workflow consists of three main steps:

 * [Update](#update)
 * [Create a Topic](#create-a-topic)
 * [Submit a Merge Request](#merge-request)

Update
------

Update your local `master` branch:

```sh
   $ git checkout master
   $ git pull
```

Create a Topic
--------------

All new work must be committed on topic branches. Name topics like you might
name functions: concise but precise. A reader should have a general idea of the
feature or fix to be developed given just the branch name.

To start a new topic branch:

```sh
   $ git fetch upstream
```

Start the topic from `upstream/master`:

```sh
   $ git checkout -b my-topic upstream/master
```

Edit files and create commits (repeat as needed).
Add a prefix to your commit message (see below).

```sh
   $ edit file1 file2 file3
```

```sh
   $ git add file1 file2 file3
   $ git commit -m "Relavent message"
```

The body of the message should clearly describe the motivation of the commit
(**what**, **why**, and **how**). In order to ease the task of reviewing
commits, the message body should follow the following guidelines:

  1. Leave a blank line between the subject and the body.
  This helps `git log` and `git rebase` work nicely, and allows to smooth
  generation of release notes.
  2. Try to keep the subject line below 72 characters, ideally 50.
  3. Capitalize the subject line.
  4. Do not end the subject line with a period.
  5. Use the imperative mood in the subject line (e.g. `Fix spacing
  not being considered.`).
  6. Wrap the body at 80 characters.
  7. Use semantic line feeds to separate different ideas, which improves the
  readability.
  8. Be concise, but honor the change: if significant alternative solutions
  were available, explain why they were discarded.
  9. If the commit refers to an issue discussed, reference the issue in the message

Keep in mind that the significant time is invested in reviewing commits and
*pull requests*, so following these guidelines will greatly help the people
doing reviews.

These guidelines are largely inspired by Chris Beam's
[How to Write a Commit Message](https://chris.beams.io/posts/git-commit/)
post.

Submit A Merge Request
----------------------

You can create a Merge Request at any point during your development.
And subsequent commits to this branch will automatically be added to the Merge Request.
LBM maintainers will communicate with you on this Merge Request thread.

**Only authorized developers with GitHub merge permissions execute this step.**

After a feature topic has been reviewed and approved in GitLab, LBM
maintainers will merge it into the upstream repository via the GitLab user
interface.

